
	// MongoDB - Query Operators
/*
	-A "query" is a request for data from database.
	-An "Operator" is a symbol that represent an action or a process
	- putting them together means the things that we can do on our queries using certain operators
*/

/*
	-knowing query operators will enable us to queries that can do more than what simple operators do. (We can do more than what we do in crud operations)
*/

// Comparison query operators

/*
	-greater than
	-less than
	-greater/less than or equal to
	-not equal to
	-in
*/

// greater than "$gt"

/*
	-finds documents that have fields number that are greater than a specific value.
	syntax:
		db.collectionName.find({field: {$gt: "value"}});
*/

	db.users.find({age: {$gt: 76}});

// greater than or equal to "$gte"
/*
	-finds documents that have field number values that are greater than or equal to specific value
	Syntax:
		db.collectionName.find({field: {$gte: "value"}});
*/

	db.users.find({age: {$gte: 76}});


// less than "$lt"
/*
	-finds documents that have field number values that is less than the specified values.
	Syntax:
		db.collectionName.find({field: {$lt: "value"}});
*/

	db.users.find({age: {$lt: 65}});


// less than or equal to "$lte"
/*
	-finds documents that have field number values that is less than or equal to the specified values.
	Syntax:
		db.collectionName.find({field: {$lte: "value"}});
*/

	db.users.find({age: {$lte: 65}});

// not equal to "$ne"
/*
	-finds documents that have field numbers values that are not equal to the specified values.
	Syntax:
		db.collectionName.find({field: {$ne: "value"}});
*/

	db.users.find({age: {$ne: 65}});

// in operator "$in"
/*
	--finds documents with specific match criteria on one field using different values.
	Syntax:
		db.collectionName.find({field: {$in: "value"}});
*/

	db.users.find({lastName: {$in: ["Hawking", "Doe"]}});
	db.users.find({courses: {$in: ["HTML", "React"]}});


// Evaluation query operators
/*
	-return data based on evaluations of either individual fields or the entire collection's document
*/

// regex operator "$regex"
/*
	-regex - regular expression
	-they are based on regular languages
	-used for matching strings
	-it allows us to find documents that match specific string pattern using regular expression
*/
	
// Case sensitive query "$regex"

	/*
	Syntax:
		-db.collectionName.find({field: {$regex: "pattern"}})
	*/

	db.users.find({lastName: {$regex: "A"}});

// Case insensitive query "$i"

/*
	-we can run case insensitive queries by utilizing the "i" option

	Syntax:
		db.collectionName.find({lastName: {$regex: pattern, $options: "optionValue"}});
*/

		db.users.find({lastName: {$regex: "A", $options: "$i"}});



// mini - activity:

	db.users.find({firstName: {$regex: "e", $options: "$i"}});


// logical query opearator
/*
	-or operator "$or"
	-finds documents that match a single criteria from multiple provided search criteria
	Syntax:
		db.collectionName.find({$or: [{fieladA: "valueA"}, {fieladB: "valueB"}]});
*/

	db.users.find({$or: [{firstName: "Neil"}, {age: "25"}]});
	db.users.find({$or: [{firstName: "Neil"}, {age: {$gte:30}}]});

// and operator "$and"
/*
	-finds document matching multiple criteria in a single field
	Syntax:
		db.collectionName.find({$and: [{fieladA: "valueA"}, {fieladB: "valueB"}]});
*/

	db.users.find({$and: [{age: {$ne: 82}}, {age: {$ne: 82}}]});

// mini - activity:

	db.users.find({$and: [{firstName: "e"}, {age: {$lte:30}}]});


// field projection

/*
	-by default, MongoDB returns the whole documents, es[ecially when dealing with complex documents
	-to help with readability of the values returned
	-or because of the security reason, we include or exclude some fields

*/

// inclusion
/*
	-allows us to include/add specific fields only when retrieving documents
	-the value denoted is "1" to indicate that the field is being included.
	-we cannot do exclusion on field that uses inclusion projection
		syntax:
		db.collectionName.find({criteria}, {field: 1});
*/
// before inclusion
	db.users.find({firstName: "Jane"});

// during inclusion
	db.users.find(
			{
				firstName: "Jane"
			},
			{
				firstName: 1,
				lastName: 1,
				"contact.phone": 1
			}
		);

// returning specific fields in embedded documents
	/*
		-double quotations are important.

		db.users.find(
			{
				firstName: "Jane"
			},
			{
				firstName: 1,
				lastName: 1,
				"contact.phone": 1
			}
		);

	*/

// -exception to the inclusion rule - suppressing the td field
	/*
		-allows us to exclude the "_id" field when retriving documents
		-when using field projection, field inclusion and exclusion may not be used at the same time.
		-excluding the "_id" field is the only exception to this rule
		Syntax:
			db.collectionName.find({criteria}, {_id:0});
	*/
	db.users.find(
			{
				firstName: "Jane"
			},
			{
				firstName: 1,
				lastName: 1,
				contact: 1,
				_id: 0

			}
		);

// slice operator "$slice"
/*
	-allows us to retrieve only 1 element that matches the search criteria

*/

db.users.insert({
	namearr:[
		{
			name: "Juan"
		},
		{
			name: "tamad"
		}
	]
});

db.users.find({
	namearr:
	{
		name: "Juan"
	}
});

// Slice operator

db.users.find(
			{ "namearr": 
				{ 
					namea: "Juan" 
				} 
			}, 
			{ namearr: 
				{ $slice: 1 } 
			}
		);

// mini- activity

	
		db.users.find(
			{
				firstName: {$regex: "s", $options: "$i"}
			},
			{
				firstName: 1,
				lastName: 1,
				_id: 0
	

			}
		);


// Exclusion
/*
	-allows us to exclude or remove specific fields when retrieving documents
	-the value provided is zero to denote that the field is being included.
	Syntax:
		db.collectionName.find({criteria}, field: 0);
*/

	db.users.find({firstName: "Jane"}, {contact: 0, department: 0});


// excluding/supressing specific fields in embedded documents


db.users.find(
{
	firstName: "Jane"
},
{
	"contact.phone": 0
}

);


